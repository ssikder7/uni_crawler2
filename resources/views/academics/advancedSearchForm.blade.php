<div class="form-group">
    <label class="control-label">Keyword</label>
    <div class="">
        <input type="text" name="search[keyword]" value="{{isset($search['keyword'])?$search['keyword']:''}}" placeholder="Search..." class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="control-label">Title</label>
    <div class="">
        <input type="text" name="search[title]" value="{{isset($search['title'])?$search['title']:''}}" placeholder="title" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="control-label">Other</label>
    <div class="">
        <input type="text" name="search[other]" value="{{isset($search['other'])?$search['other']:''}}" placeholder="other" class="form-control">
    </div>
</div>
<div class="form-group">
    <label class="control-label">Qualifications</label>
    <div class="">
        <input type="text" name="search[qualifications]" value="{{isset($search['qualifications'])?$search['qualifications']:''}}" placeholder="qualifications" class="form-control">
    </div>

</div>
<div class="form-group">
    <label class="control-label">University</label>
    <div class="">
        {{ Form::select('search[university][]', $universities, isset($search['university'])?$search['university']:[], ["class"=>"form-control university_dropdown", 'multiple' => 'multiple'])}}
    </div>
</div>
<div class="form-group">
<div class="col-md-offset-4 col-md-4">
    <button class="btn btn-default" type="submit">
        <i class="fa fa-search">Search</i>
    </button>
    </div>
</div>

