@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">Academics Statistics</div>

                    <div class="panel-body">
                        <div class="stats container-fluid panel">
                        @foreach($stats as $name => $count)
                        <div class="row">
                        <p class="stats-name col-md-4">{{$name}}</p><p class="stats-count col-md-4">{{$count}}</p>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
