<div> {{$academics->count()}} out of {{$academics->total()}}</div>
<br/>
<div class="table-responsive">
    <table class="table table-borderless">
        <thead>
            <tr>
                <th>Name</th>
                <th>URL</th>
                <th>University</th>
                <th>Title</th>
                <th>Qualifications</th>
                {{--<th>Publications</th>--}}
                {{--<th>Other</th>--}}
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($academics as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>
                @foreach(explode(';', $item->url) as $index => $url)
                <a href="{{trim($url)}}" target="_blank">[URL{{$index+1}}]</a>
                @endforeach
                </td>
                <td>{{ $item->university_name }}</td>
                <td>{{ str_limit($item->title, $limit = 100, $end = '...') }}</td>
                <td>{{ str_limit($item->qualifications, $limit = 100, $end = '...') }}</td>
                {{--<td>{{ str_limit($item->publication_record, $limit = 100, $end = '...') }}</td>--}}
                {{--<td>{{ str_limit($item->other, $limit = 100, $end = '...') }}</td>--}}
                <td>
                    {{--<a href="{{ url('/academics/' . $item->id) }}" title="View academic"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>--}}
                    <a href="{{ url('/academics/' . $item->id . '/edit') }}" title="Edit academic"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/academics', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete academic',
                                'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination-wrapper"> {!! $academics->appends(['search' => Request::get('search')])->render() !!} </div>
</div>