@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">Academics</div>
                    <div class="panel-body">
                        <a href="{{ url('/academics/create') }}" class="btn btn-success btn-sm" title="Add New academic">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <a href="{{ url('/academics/advancedSearch') }}" class="advanced-search-link">Advanced search</a>
                        {!! Form::open(['method' => 'GET', 'url' => '/', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" value="{{$keyword}}" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search">Search</i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                        <br/>
                        <br/>
@include ('academics.grid')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
