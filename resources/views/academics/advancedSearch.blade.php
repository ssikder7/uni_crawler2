@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">Advanced Search Academics</div>
                    <div class="panel-body">
                        <div class="col-md-3">
                        {!! Form::open(['method' => 'GET', 'url' => url('/academics/advancedSearch') , 'class' => 'form-vertical'])  !!}
                            @include ('academics.advancedSearchForm')
                        {!! Form::close() !!}
                        </div>
                        <br/>
                        <div class="col-md-9">
                        @include ('academics.grid')
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
