@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">Failed Urls</div>
                    <div class="panel-body">
                        <br/>
                        <div> {{$urls->count()}} out of {{$urls->total()}}</div>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>URL</th>
                                        <th>Reason</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($urls as $item)
                                    <tr>
                                        <td><a href="{{$item->url}}" target="_blank">{{$item->url}}</a></td>
                                        <td>{{ str_limit($item->reason, $limit = 100, $end = '...') }}</td>
                                        <td>
                                            <a href="{{ url('/failed/recrawl/' . $item->id) }}" title="ReCrawl"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> ReCrawl</button></a>
                                            <a href="{{ url('/failed/delete/' . $item->id) }}" title="Delete"><button class="btn btn-danger btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Delete</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $urls->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
