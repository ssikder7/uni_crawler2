<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'AcademicsController@index');
Route::get('/academics/recrawl/{id}', 'AcademicsController@recrawl');
Route::get('/academics/advancedSearch', 'AcademicsController@advancedSearch');
Route::get('/statistics', 'AcademicsController@statistics');
Route::resource('academics', 'AcademicsController');
Route::resource('failed', 'FailedController');
Route::get('/failed/delete/{id}', 'FailedController@delete');
Route::get('/failed/recrawl/{id}', 'FailedController@recrawl');
