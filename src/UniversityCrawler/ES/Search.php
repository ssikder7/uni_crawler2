<?php

namespace UniversityCrawler\ES;

use Cviebrock\LaravelElasticsearch\Facade;

class Search
{

    const AND_OPERATOR_SEPARATOR = " AND ";

    private $indexedColumns = [
        'name',
        'other',
        'title',
        'qualifications'
    ];

    public function globalAcademicSearchByKeyword($keyword)
    {
        $params = [
            'index' => 'academics',
            'type' => 'academics',
            'body' => [
                "fields" => ["id"],
                'query' => [
                    'query_string' => ["query" => $keyword]
                ],
                'from' => 0,
                'size' => 500
            ]
        ];

        return Facade::search($params);
    }

    public function advancedAcademicSearch($search)
    {
        $queryBool = [];

        $queryBool = array_merge_recursive($queryBool, $this->getFieldsSearchQuery($search));

        if (isset($search['university'])) {
            $queryBool['must'][] = ['bool' => $this->getUnivercitySearchQuery($search['university'])];
        }

        if (isset($search['keyword'])) {
            $keywordSearchQuery = $this->getKeywordSearchQuery($search['keyword']);
            if (isset($queryBool['must']) && count($queryBool['must'])) {
                $queryBool['must'][] = ['bool' => $keywordSearchQuery];
            } else {
                $queryBool = $keywordSearchQuery;
            }
        }

        //print_r($queryBool);exit;

        $params = [
            'index' => 'academics',
            'type' => 'academics',
            'body' => [
                "fields" => ["id"],
                'query' => [
                    'bool' => $queryBool
                ],
                'from' => 0,
                'size' => 500
            ]
        ];

        return Facade::search($params);
    }

    private function getFieldsSearchQuery($paramsByFields)
    {
        $must = [];
        foreach ($this->indexedColumns as $column) {
            if (!isset($paramsByFields[$column])) {
                continue;
            }

            if ($this->isAndOperatorSearch($paramsByFields[$column])) {
                $keywords = $this->getAndSeaparatedKeywords($paramsByFields[$column]);
            } else {
                $keywords = [$paramsByFields[$column]];
            }

            foreach ($keywords as $keyword) {
                $cleanKeyword = $this->cleanKeyword($keyword);
                $type = $this->getMatchType($keyword);
                $must[] = [$type => [$column => $cleanKeyword]];
            }
        }

        return ['must' => $must];
    }

    /**
     * @param $keyword
     * @return string
     */
    private function getKeywordSearchQuery($keyword)
    {
        $type = $this->getMatchType($keyword);
        $keyword = $this->cleanKeyword($keyword);

        $should = [];

        foreach ($this->indexedColumns as $column) {
            $should[] = [$type => [$column => $keyword]];
        }

        return ['should' => $should];
    }


    private function getUnivercitySearchQuery($university)
    {
        $query = [];
        foreach ($university as $singleUniversity) {
            $query[] = ['match_phrase' => ['university_name' => $singleUniversity]];
        }
        return ['should' => $query];
    }

    private function isAndOperatorSearch($keyword)
    {
        $parts = explode(self::AND_OPERATOR_SEPARATOR, $keyword);
        return count($parts) > 1;
    }

    private function getAndSeaparatedKeywords($keyword)
    {
        $keywords = [];
        $parts = explode(self::AND_OPERATOR_SEPARATOR, $keyword);
        foreach($parts as $part) {
            if (trim($part)) {
                $keywords[] = trim($part);
            }
        }

        return $keywords;
    }

    private function cleanKeyword($keyword)
    {
        if ($this->isExactMatchSearch($keyword)) {
            return trim(trim($keyword, '"'));
        }
        return trim($keyword);
    }

    private function isExactMatchSearch($string)
    {
        return substr($string, 0, 1) == '"' && substr($string, -1) == '"';
    }

    private function getMatchType($keyword)
    {
        if ($this->isExactMatchSearch($keyword)) {
            return "match_phrase";
        }
        return "match";
    }


}