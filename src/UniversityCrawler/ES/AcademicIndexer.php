<?php

namespace UniversityCrawler\ES;


use App\Academics;
use Cviebrock\LaravelElasticsearch\Facade;

class AcademicIndexer
{
    public static function index(Academics $academic)
    {
        $indexData = [
            'body' => [
                'id' => $academic->id,
                'name' => strtolower($academic->name),
                'university_name' => strtolower($academic->university_name),
                'qualifications' => strtolower($academic->qualifications),
                'faculty_department_name' => strtolower($academic->faculty_department_name),
                'publication_record' => strtolower($academic->publication_record),
                'title' => strtolower($academic->title),
                'other' => strtolower($academic->other)
            ],
            'index' => 'academics',
            'type' => 'academics',
            'id' => $academic->id,
        ];

        self::delete($academic->id);

        Facade::index($indexData);
    }

    public static function delete($id)
    {
        $deleteData = [
            'index' => 'academics',
            'type' => 'my_type',
            'id' => $id,
        ];

        if (Facade::exists($deleteData)) {
            Facade::delete($deleteData);
        }
    }
}