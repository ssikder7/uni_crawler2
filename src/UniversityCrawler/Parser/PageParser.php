<?php

namespace UniversityCrawler\Parser;

use PHPHtmlParser\CurlInterface;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\LongTimeoutCurl;

class PageParser
{
    /**
     * @var Dom
     */
    protected $dom;

    /**
     * @var CurlInterface
     */
    protected $curl;

    /**
     * @var bool
     */
    protected $debug;

    public function __construct(Dom $dom = null, CurlInterface $curl = null, $debug = false)
    {
        $this->dom = $dom ?: new Dom();
        $this->curl = $curl ?: new LongTimeoutCurl();
        $this->debug = $debug;
    }

    public static function log($message, $debug = false)
    {
        if ($debug) {
            print_r($message . "\n");
        }
    }
}