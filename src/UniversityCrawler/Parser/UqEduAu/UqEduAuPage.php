<?php

namespace UniversityCrawler\Parser\UqEduAu;

use UniversityCrawler\Parser\PageParser;

class UqEduAuPage extends PageParser
{
    protected $mapping = [
        'name' => [
            'type' => 'dom',
            'path' => 'div.name-holder h2',
        ],

        'title' => [
            'div.appointment h4',
            'div.appointment div:nth-child(2) span'
        ],
        'qualifications' => [
            'path' => 'div.qualifications'
        ],
        'faculty_department_name' => [
            'path' => 'div.appointment div:nth-child(3) span'
        ],
        'phone_number' => [
            'path' => 'div.contact-holder span'
        ],
        'publication_record' => [
            'path' => '#publications'
        ],
        'other' => [
            'Overview' => '#overview',
            'Research Projects' => '#research-projects',
            'PhD and MPhil Supervision' => 'supervision',
        ]

    ];

    private $url;

    public function parse($url)
    {
        $this->url = $url;

        PageParser::log("Processing $url", $this->debug);
        $this->dom->loadFromUrl($url, [], $this->curl);

        $data = [
            'university_name' => 'The University of Queensland',
            'url' => $url,
            'type' => UqEduAuCrawler::TYPE
        ];

        foreach ($this->mapping as $name => $options) {
            //phone_number => getPhoneNumber
            $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

            if (method_exists($this, $methodName)) {
                $data[$name] = $this->$methodName();
            } else {
                throw new \Exception('No method for getting ' . $name);
            }
        }

        return $data;
    }

    private function getFromDom($path)
    {
        $data = [];
        $elements = $this->dom->find($path);
        foreach ($elements as $element) {
            $data[] = strip_tags(str_replace('>', '> ', $element->outerHtml));
        }

        return $data;
    }

    private function getName()
    {
        $name = $this->getSingleDomItemByPath($this->mapping['name']['path']);
        if (!$name) {
            throw new \Exception('No name for ' . $this->url);
        }

        return $this->prepareString($name);
    }

    private function getQualifications()
    {
        return $this->prepareString($this->getSingleDomItemByPath($this->mapping['qualifications']['path']));
    }

    private function getTitle()
    {
        $data = [];
        foreach ($this->mapping['title'] as $path) {
            $data[] = $this->prepareString($this->getSingleDomItemByPath($path));
        }

        return $this->prepareString(implode('; ', $data));
    }

    private function getPhoneNumber()
    {
        return $this->prepareString($this->getSingleDomItemByPath($this->mapping['phone_number']['path']));
    }

    private function getSingleDomItemByPath($path)
    {
        $item = $this->getFromDom($path);
        return isset($item[0]) ? $item[0] : '';
    }

    private function getFacultyDepartmentName()
    {
        return $this->prepareString($this->getSingleDomItemByPath($this->mapping['faculty_department_name']['path']));
    }

    private function getPublicationRecord()
    {
        return $this->prepareString($this->getSingleDomItemByPath($this->mapping['publication_record']['path']));
    }

    public function getOther()
    {
        $data = [];
        foreach ($this->mapping['other'] as $name => $path) {
            $data[] = $this->prepareString($this->getSingleDomItemByPath($path));
        }

        return $this->prepareString(implode('; ', $data));
    }

    private function prepareString($string, $max = 65000)
    {
        $string = strip_tags($string);
        $string = str_replace("  ", ' ', $string);
        $string = str_replace("  ", ' ', $string);
        $string = html_entity_decode($string);
        $string = htmlspecialchars_decode($string);

        return substr($string, 0, $max);
    }

}

