<?php

namespace UniversityCrawler\Parser\UqEduAu;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class UqEduAuDirectoryPage extends PageParser
{
    public function getFieldsOfResearchLinks()
    {
        PageParser::log("Getting list of Fields Of Research Links", $this->debug);

        $url = "http://researchers.uq.edu.au/fields-of-research";

        $links = [];
        $this->dom->loadFromUrl($url, [], $this->curl);
        $linkElements = $this->dom->find('tbody tr td a');

        if (!count($linkElements)) {
            return [];
        }

        /** @var HtmlNode $element */
        foreach ( $linkElements as $element ) {
            $link = $element->getAttribute('href');
            $links[] = "http://researchers.uq.edu.au" . trim($link);
        }

        PageParser::log(count($links) . " links found", $this->debug);

        return $links;
    }

    public function getOrganizationsLinks()
    {
        PageParser::log("Getting list of Organizations Links", $this->debug);

        $url = "http://researchers.uq.edu.au/organisations";

        $links = [];
        $this->dom->loadFromUrl($url, [], $this->curl);
        $linkElements = $this->dom->find('tbody tr td a');

        if (!count($linkElements)) {
            return [];
        }

        /** @var HtmlNode $element */
        foreach ( $linkElements as $element ) {
            $link = $element->getAttribute('href');
            $links[] = "http://researchers.uq.edu.au" . trim($link);
        }

        PageParser::log(count($links) . " links found", $this->debug);

        return $links;
    }

    public function getAcademicUrls($fieldOfResearchUrl)
    {
        PageParser::log("Getting list of academics urls for " . $fieldOfResearchUrl, $this->debug);

        $links = [];
        $this->dom->loadFromUrl($fieldOfResearchUrl, [], $this->curl);
        $linkElements = $this->dom->find('tbody tr td a');

        if (!count($linkElements)) {
            return [];
        }

        /** @var HtmlNode $element */
        foreach ( $linkElements as $element ) {
            $link = $element->getAttribute('href');
            if (strpos($link , "/researcher/") === 0) {
                $links[] = "http://researchers.uq.edu.au" . trim($link);
            }
        }

        return array_unique($links);
    }
}