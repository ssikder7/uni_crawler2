<?php

namespace UniversityCrawler\Parser\UqEduAu;

use App\Academics;
use App\FailedUrls;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class UqEduAuCrawler
{
    const TYPE = 'uqeduau';

    /**
     * @var UqEduAuDirectoryPage
     */
    private $uqEduAuDirectoryPage;
    /**
     * @var UqEduAuPage
     */
    private $uqEduAu;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(UqEduAuDirectoryPage $uqEduAuDirectoryPage, UqEduAuPage $uqEduAuPage, $debug = false)
    {
        $this->uqEduAuDirectoryPage = $uqEduAuDirectoryPage;
        $this->uqEduAu = $uqEduAuPage;
        $this->debug = $debug;
    }

    public function process($link)
    {
        $links = $this->uqEduAuDirectoryPage->getAcademicUrls($link);

        if (empty($links)) {
            PageParser::log('No links found', $this->debug);
            throw new NoDataException();
        }
        PageParser::log(count($links) . ' links found', $this->debug);

        foreach ($links as $link) {
            try {
                $exists = Academics::where('url', $link)->get();
                if (count($exists)) {
                    continue;
                }
                $this->saveToDb($this->uqEduAu->parse($link));
            } catch (\Exception $e) {
                $this->saveFailedUrl($link, self::TYPE, $e->getMessage());
                PageParser::log($e->getMessage(), $this->debug);
            }
        }
    }

    private function saveToDb($data)
    {
        $academic = new Academics();
        $academic->setRawAttributes($data);
        $academic->save();
    }

    private function saveFailedUrl($url, $type, $reason)
    {
        $failedUrl = new FailedUrls();
        $failedUrl->url = $url;
        $failedUrl->type = $type;
        $failedUrl->reason = substr($reason, 0, 10000);
        $failedUrl->save();
    }
}
