<?php

namespace UniversityCrawler\Parser;

use UniversityCrawler\Parser\Qut\QutPage;
use UniversityCrawler\Parser\UnOfAdelaida\UnOfAdelaidaPage;
use UniversityCrawler\Parser\UqEduAu\UqEduAuPage;

class ParserFactory
{
    public static function getParser($type)
    {
        switch ($type) {
            case 'qut':
                return new QutPage();
            case 'unofadelaida':
                return new UnOfAdelaidaPage();
            case 'uqeduau' :
                return new UqEduAuPage();
        }

        throw new \Exception('No parser for type ' . $type);
    }
}