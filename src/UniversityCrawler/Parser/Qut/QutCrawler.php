<?php

namespace UniversityCrawler\Parser\Qut;

use App\Academics;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class QutCrawler
{
    /**
     * @var QutDirectoryPage
     */
    private $qutDirectoryPage;
    /**
     * @var QutPage
     */
    private $qutPage;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(QutDirectoryPage $qutDirectoryPage, QutPage $qutPage, $debug = false)
    {
        $this->qutDirectoryPage = $qutDirectoryPage;
        $this->qutPage = $qutPage;
        $this->debug = $debug;
    }

    public function process($letter, $page)
    {
        $url = "http://staff.qut.edu.au/?query=$letter&page=$page";

        $links = $this->qutDirectoryPage->getLinks($url);
        if (empty($links)) {
            PageParser::log('No links found', $this->debug);
            throw new NoDataException();
        }
        PageParser::log(count($links) . ' links found', $this->debug);

        foreach ($links as $link) {
            try {
                $this->saveToDb($this->qutPage->parse($link));

            } catch (\Exception $e) {
                PageParser::log($e->getMessage(), $this->debug);
            }
        }
    }

    private function saveToDb($data)
    {
        $academic = new Academics();
        $academic->setRawAttributes($data);
        $academic->type = 'qut';
        $academic->save();
    }
}
