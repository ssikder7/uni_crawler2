<?php

namespace UniversityCrawler\Parser\Qut;

use stringEncode\Exception;
use UniversityCrawler\Parser\PageParser;

class QutPage extends PageParser
{
    protected $mapping = [
        'name' => [
            'type' => 'dom',
            'path' => '.personal-details span.fn',
        ],
        'title' => [
            'title_role' => [
                'type' => 'dom',
                'path' => '.personal-details .role',
            ],
            'title_org' => [
                'type' => 'dom',
                'path' => '.personal-details .org',
            ],
        ],
        'qualifications' => [
            'type' => 'regex',
            'regex' => '/Qualifications(.*?)detail-group"> <p>(.*?)<\/p/'
        ],
        'faculty_department_name' => [
            'type' => 'dom',
            'path' => '.profile-staff-orgarea'
        ],
        'phone_number' => [
            'type' => 'dom',
            'path' => '.personal-details .tel'
        ],
        //TODO email
        'publication_record' => [
            'type' => 'dom',
            'path' => '.publications-list'
        ],
        'other' => [
            'biography' => '.biography',
            'experience' => '.experience',
            'awards' => '.awards',
            'projects' => '.projects',
            'supervision' => '.supervision',
            'teaching' => '.teaching'
        ]
    ];

    public function parse($url)
    {
        PageParser::log("Processing $url", $this->debug);
        $this->dom->loadFromUrl($url, [], $this->curl);

        $data = [
            'university_name' => 'Queensland University of Technology',
            'url' => $url
        ];

        foreach ($this->mapping as $name => $options) {
            //phone_number => getPhoneNumber
            $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

            if (method_exists($this, $methodName)) {
                $data[$name] = $this->$methodName();
            } else {
                throw new Exception('No method for getting ' . $name);
            }
        }

        return $data;
    }

    private function getFromDom($path)
    {
        $data = [];
        $elements = $this->dom->find($path);
        foreach ($elements as $element) {
            $data[] = strip_tags(str_replace('>', '> ', $element->outerHtml));
        }

        return $data;
    }

    private function getName()
    {
        return $this->getSingleDomItemByPath($this->mapping['name']['path']);
    }

    private function getQualifications()
    {
        $html = $this->dom->outerHtml;
        $html = preg_replace( "/\r|\n/", "", $html);

        $regex = $this->mapping['qualifications']['regex'];

        if (preg_match($regex, $html, $matches)) {
            return $matches[2];
        } else {
            return '';
        }
    }

    private function getTitle()
    {
        $data = [];
        foreach ($this->mapping['title'] as $name => $parts) {
            $data[$name] = $this->getFromDom($parts['path']);
        }
        $data = array_values($data);

        $str = [];
        foreach ($data[0] as $i => $part) {
            $str[] = $part . ': ' . $data[1][$i];
        }

        return implode('; ', $str);
    }

    private function getPhoneNumber()
    {
        return $this->getSingleDomItemByPath($this->mapping['phone_number']['path']);
    }

    private function getSingleDomItemByPath($path)
    {
        $item = $this->getFromDom($path);
        return isset($item[0]) ? $item[0] : '';
    }

    private function getFacultyDepartmentName()
    {
        return $this->getSingleDomItemByPath($this->mapping['faculty_department_name']['path']);
    }

    private function getPublicationRecord()
    {
        return $this->getSingleDomItemByPath($this->mapping['publication_record']['path']);
    }

    public function getOther()
    {
        $data = [];
        foreach ($this->mapping['other'] as $name => $path) {
            $el = $this->getFromDom($path);
            if (isset($el[0])) {
                $data[] = strtoupper($name) . ": " . $el[0];
            }
        }

        $html = $this->dom->outerHtml;
        $html = preg_replace( "/\r|\n/", "", $html);

        //Professional memberships and associations
        $regex = '/Start BUDDYPRESS fields -->(.*?)<\/dt>(.*?)<!-- End BUDDYPRESS/';
        if (preg_match($regex, $html, $matches)) {
            $data[] = strtoupper('Professional memberships and associations') . ": " . strip_tags($matches[3]);
        }

        //keywords
        $regex = '/<meta name="keywords" content="(.*?)" \/>/';
        if (preg_match($regex, $html, $matches)) {
            $data[] = strtoupper('Keywords') . ": " . strip_tags($matches[1]);
        }

        return implode('; ', $data);
    }
}

