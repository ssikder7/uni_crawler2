<?php

namespace UniversityCrawler\Parser\Qut;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class QutDirectoryPage extends PageParser
{
    public function getLinks($directoryUrl)
    {
        $links = [];
        $this->dom->loadFromUrl($directoryUrl, [], $this->curl);
        $linkElements = $this->dom->find('.results-span-profile-header a');

        if (!count($linkElements)) {
            return [];
        }

        /** @var HtmlNode $element */
        foreach ( $linkElements as $element ) {
            $links[] = $element->getAttribute('href');
        }

        return $links;
    }
}