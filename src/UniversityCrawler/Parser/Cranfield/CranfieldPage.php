<?php

namespace UniversityCrawler\Parser\Cranfield;

use stringEncode\Exception;
use UniversityCrawler\Parser\PageParser;

class CranfieldPage extends PageParser
{
    protected $mapping = [
      'name' => [],
      'title' => [],
      'faculty_department_name' => [],
      'phone_number' => [],
      'email' => [],
      'publication_record' => [],
    ];

    protected $xpath;

    public function parse($url)
    {
        PageParser::log("Processing $url", $this->debug);
        //$this->dom->loadFromUrl($url, [], $this->curl);

        //init DOMXpath
        libxml_use_internal_errors(true);
        libxml_disable_entity_loader(true);
        $doc = new DOMDocument();
        $curl = new LongTimeoutCurl();
        $return = $curl->get($url);
        $doc->loadHTML($return);
        $this->xpath = new DOMXpath($doc);

        $data = [
          'university_name' => 'Cranfield University',
          'url' => $url
        ];

        foreach ($this->mapping as $name => $options)
        {
            //phone_number => getPhoneNumber
            $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

            if (method_exists($this, $methodName))
            {
              $data[$name] = $this->$methodName();
            }
            else
            {
              throw new Exception('No method for getting ' . $name);
            }
        }

      return $data;
    }

    private function getFacultyDepartmentName()
    {
        $facaulty = $this->xpath->query("//*[contains(@class, 'profile-text')]");
        if($facaulty->length == 0 ) return '';
        $Facaulty = $facaulty->item(0)->childNodes->item(6)->textContent;
        return $Facaulty;

    }

    private function getName()
    {
        $name = $this->xpath->query("//*[contains(@class, 'profile-text')]");
        if($name->length == 0 ) return '';
        $name =  $name->item(0)->childNodes->item(2)->textContent;
        return $name;
    }

   private function getTitle()
   {
        $title = $this->xpath->query("//*[contains(@class, 'profile-text')]");
        if($title->length == 0 ) return '';
        $Title = $title->item(0)->childNodes->item(4)->textContent;
        return $Title;
    }

    private function getEmail() {
        $email = $this->xpath->query("//*[contains(@class, 'snapshot')]");
        if($email->length == 0 ) return '';
        $email = $email->item(0)->childNodes->item(2)->textContent;
        $email = substr( $email, 7);
        return $email;
    }

    private function getPhoneNumber() {
        $phone = $this->xpath->query("//*[contains(@class, 'snapshot')]");
        if($phone->length == 0 ) return '';
        $phone = $phone->item(0)->childNodes->item(0)->textContent;
        $phone = substr( $phone, 4);
        return $phone;
    }

    private function getPublicationRecord()
    {
        $innerjournals = $this->xpath->query("//*[contains(@class, 'tab-content listing-article')]");
        if($innerjournals->length == 0 ) return '';
        $innerlength = $innerjournals->length;
        for ($j=0; $j < $innerlength; $j++)
        {
            $innLen = $innerjournals->item($j)->childNodes->item(3)->childNodes->length;
            //print_r($innLen);
            for ($k=0;$k<$innLen;$k++)
            {
                $journal[]= $innerjournals->item($j)->childNodes->item(3)->childNodes->item($k)->textContent;
            }
        }
        return $journal;
    }
}


?>
