<?php

namespace UniversityCrawler\Parser\Cranfield;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class CranfieldDirectoryPage extends PageParser
{
    protected $xpath;
    public function getLinks($url)
    {
      	$links = array();
      	while(1)
        {
      			$url2 =$url;
      			libxml_use_internal_errors(true);
      			libxml_disable_entity_loader(true);
      			$doc = new DOMDocument();
            $curl = new LongTimeoutCurl();
            $return = $curl->get($url2);
      			$doc->loadHTML($return);
      			$xpath = new DOMXpath($doc);

      			$url = $xpath->query("//*[contains(@class, 'listing-main')]");
      			$domain = 'http://search.cranfield.ac.uk';
      			$length = $url->item(0)->childNodes->item(1)->childNodes->length;

      			for ($i=0; $i <$length ; $i=$i+2)
      			{
      				$output1 =$url->item(0)->childNodes->item(1)->childNodes->item($i)->childNodes->item(1)->attributes->item(0)->textContent;
      				$url1 = $domain . $output1;
      				$links[] = $domain . $output1;
      			}

      			$url = $xpath->query("//*[contains(@class, 'pagination')]");
      			$domain = 'http://search.cranfield.ac.uk/s/';
      			$length = $url->item(0)->childNodes->length;
      			$nextLink = $url->item(0)->childNodes->item($length-2)->childNodes->item(0)->attributes->item(0)->textContent;

      			$nextLink = $domain . $nextLink ;

      			if( $nextLink ==$url2)
      				BREAK;
      			$url = $nextLink;
        }

        return $links;
    }
}


?>
