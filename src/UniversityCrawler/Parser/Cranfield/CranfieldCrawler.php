<?php

namespace UniversityCrawler\Parser\Cranfield;

use App\Academics;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class CranfieldCrawler
{
    /**
     * @var CranfieldDirectoryPage
     */
    private $CranfieldDirectoryPage;
    /**
     * @var CranfieldPage
     */
    private $CranfieldPage;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(QutDirectoryPage $CranfieldDirectoryPage, QutPage $CranfieldPage, $debug = false)
    {
        $this->CranfieldDirectoryPage = $CranfieldDirectoryPage;
        $this->CranfieldPage = $CranfieldPage;
        $this->debug = $debug;
    }

    public function process()
    {

        $url = "http://search.cranfield.ac.uk/s/search.html?collection=cranfield-people&profile=_default&query=";
        
        $links = $this->CranfieldDirectoryPage->getLinks($url);
        if (empty($links)) 
        {
            PageParser::log('No links found', $this->debug);
            throw new NoDataException();
        }
        PageParser::log(count($links) . ' links found', $this->debug);

        foreach ($links as $link) 
        {
            try 
            {
                $this->saveToDb($this->UnsiaPage->parse($link));

            } 
            catch (\Exception $e) 
            {
                PageParser::log($e->getMessage(), $this->debug);
            }
        }
    }

    private function saveToDb($data)
    {
        $academic = new Academics();
        $academic->setRawAttributes($data);
        $academic->type = 'Unsia';
        $academic->save();
    }
}
