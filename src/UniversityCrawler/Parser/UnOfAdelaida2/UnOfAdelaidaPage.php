<?php

namespace UniversityCrawler\Parser\UnOfAdelaida2;

use UniversityCrawler\Parser\PageParser;

class UnOfAdelaidaPage extends PageParser
{
    protected $mapping = [
        'name' => 'span.field--name-title',
        'title' => "div.field--name-field-ua-rp-position",
        'qualifications' => '#qualifications',
        'faculty_department_name' => [
            'div.profile-block__school',
            'div.profile-block__faculty',
            '.field--name-field-ua-rp-campus div.field__item',
            '.field--name-field-ua-rp-building div.field__item',
            '.field--name-field-ua-rp-room-number div.field__item'
        ],
        'email' => 'a.sprite-email',
        'phone_number' => '.field--type-telephone div.field__item',
        'publication_record' => '#publications',
        'other' => [
            'Bio' => 'div.field-name-field-ua-rp-bio',
            'Research' => 'div.field--name-field-ua-rp-my-research',
            'Career' => '#career',
            'Grants and Funding' => '#grants-and-funding',
            'Teaching' => '#teaching',
            'Supervision' => '#supervision',
            'Professional activities' => '#professional-activities'
        ]
    ];

    private $url;

    public function parse($url)
    {
        $this->url = $url;

        PageParser::log("Processing $url", $this->debug);
        $this->dom->loadFromUrl($url, [], $this->curl);

        $data = [
            'university_name' => 'The University of Adelaide',
            'url' => $url,
            'type' => UnOfAdelaidaCrawler::TYPE
        ];

        foreach ($this->mapping as $name => $options) {
            //phone_number => getPhoneNumber
            $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

            if (method_exists($this, $methodName)) {
                $data[$name] = $this->$methodName();
            } else {
                throw new \Exception('No method for getting ' . $name);
            }
        }

        return $data;
    }

    private function getFromDom($path)
    {
        $data = [];
        $elements = $this->dom->find($path);
        foreach ($elements as $element) {
            $data[] = strip_tags(str_replace('>', '> ', $element->outerHtml));
        }

        return $data;
    }

    private function getName()
    {
        $name = $this->getSingleDomItemByPath($this->mapping['name']);
        if (!$name) {
            throw new \Exception('No name for ' . $this->url);
        }

        return $this->prepareString($name);
    }

    private function getQualifications()
    {
        return $this->prepareString(
            $this->getSingleDomItemByPath($this->mapping['qualifications'])
        );
    }

    private function getEmail()
    {
        return $this->prepareString(
            $this->getSingleDomItemByPath($this->mapping['email'])
        );
    }

    private function getTitle()
    {
        return $this->prepareString(
            $this->getSingleDomItemByPath($this->mapping['title'])
        );
    }

    private function getPhoneNumber()
    {
        return $this->prepareString(
            $this->getSingleDomItemByPath($this->mapping['phone_number'])
        );
    }

    private function getSingleDomItemByPath($path)
    {
        $item = $this->getFromDom($path);
        return isset($item[0]) ? $item[0] : '';
    }

    private function getFacultyDepartmentName()
    {
        $data = [];

        foreach($this->mapping['faculty_department_name'] as $path) {
            $item = $this->prepareString($this->getSingleDomItemByPath($path));
            if ($item) {
                $data[] = $item;
            }
        }

        return $this->prepareString(implode("; ", $data));

    }

    private function getPublicationRecord()
    {
        return $this->prepareString(
            $this->getSingleDomItemByPath($this->mapping['publication_record'])
        );
    }

    public function getOther()
    {
        $data = [];

        foreach($this->mapping['other'] as $name => $path) {
            $item = $this->prepareString($this->getSingleDomItemByPath($path));
            if ($item) {
                $data[] =  $name . ": " . $this->prepareString($item);
            }
        }

        return $this->prepareString(implode('; ', $data));
    }

    private function prepareString($string, $max = 650000)
    {
        $string = strip_tags($string);
        $string = str_replace("  ", ' ', $string);
        $string = str_replace("  ", ' ', $string);

        return substr($string, 0, $max);
    }
}

