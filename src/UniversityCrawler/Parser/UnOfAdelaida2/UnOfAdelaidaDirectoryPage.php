<?php

namespace UniversityCrawler\Parser\UnOfAdelaida2;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class UnOfAdelaidaDirectoryPage extends PageParser
{
    public function getLinks($directoryUrl)
    {
        $links = [];
        $this->dom->loadFromUrl($directoryUrl, [], $this->curl);
        $linkElements = $this->dom->find('h2.c-description-panels__title a.c-description-panels__title-link');

        if (!count($linkElements)) {
            return [];
        }

        /** @var HtmlNode $element */
        foreach ( $linkElements as $element ) {
            $link = $element->getAttribute('href');
            $links[] = "https://researchers.adelaide.edu.au" . trim($link);
        }

        $links = array_unique($links, SORT_STRING);
        sort($links);

        return $links;
    }
}