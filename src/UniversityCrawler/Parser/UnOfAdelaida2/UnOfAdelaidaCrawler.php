<?php

namespace UniversityCrawler\Parser\UnOfAdelaida2;

use App\Academics;
use App\FailedUrls;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class UnOfAdelaidaCrawler
{
    const TYPE = 'unofadelaida';

    /**
     * @var UnOfAdelaidaDirectoryPage
     */
    private $unOfAdelaidaDirectoryPage;
    /**
     * @var UnOfAdelaidaPage
     */
    private $unOfAdelaida;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(UnOfAdelaidaDirectoryPage $unOfAdelaidaDirectoryPage, UnOfAdelaidaPage $unOfAdelaidaPage, $debug = false)
    {
        $this->unOfAdelaidaDirectoryPage = $unOfAdelaidaDirectoryPage;
        $this->unOfAdelaida = $unOfAdelaidaPage;
        $this->debug = $debug;
    }

    public function process($page)
    {
        $url = "https://researchers.adelaide.edu.au/?page=".$page;
        PageParser::log($url, $this->debug);

        $links = $this->unOfAdelaidaDirectoryPage->getLinks($url);

        if (empty($links)) {
            PageParser::log('No links found', $this->debug);
            throw new NoDataException();
        }
        PageParser::log(count($links) . ' links found', $this->debug);

        foreach ($links as $link) {
            try {
                $exists = Academics::where('url', 'like' , "%".$link.'%')->get();
                if (count($exists)) {
                    continue;
                }
                $this->saveToDb($this->unOfAdelaida->parse($link));
            } catch (\Exception $e) {
                $this->saveFailedUrl($link, self::TYPE, $e->getMessage());
                PageParser::log($e->getMessage(), $this->debug);
            }
        }
    }

    private function saveToDb($data)
    {
        $academic = Academics::where(['name' => $data['name'], 'type' => $data['type']])->first();
        if ($academic) {
            $this->addNewDataTo($academic, $data);
        } else {
            $academic = new Academics();
            $academic->setRawAttributes($data);
        }

        $academic->save();
    }

    private function saveFailedUrl($url, $type, $reason)
    {
        if (count(FailedUrls::where('url', $url)->get())) {
            return;
        }
        $failedUrl = new FailedUrls();
        $failedUrl->url = $url;
        $failedUrl->type = $type;
        $failedUrl->reason = $reason;
        $failedUrl->save();
    }

    private function addNewDataTo(Academics $academic, $data)
    {
        $academic->title .= '; ' . $data['title'];
        $academic->qualifications .= '; ' . $data['qualifications'];
        $academic->faculty_department_name .= '; ' . $data['faculty_department_name'];
        $academic->phone_number .= '; ' . $data['phone_number'];
        $academic->email .= '; ' . $data['email'];
        $academic->publication_record .= '; ' . $data['publication_record'];
        $academic->other .= '; ' . $data['other'];
        $academic->url .= '; ' . $data['url'];
    }
}
