<?php

namespace UniversityCrawler\Parser\Unsia;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class UnsiaDirectoryPage extends PageParser
{
    protected $xpath;
    public function getLinks($url)
    {
      	$links = array();
      	while(1)
        {
    			$url3 = $url;
    			libxml_use_internal_errors(true);
    			libxml_disable_entity_loader(true);
    			$doc = new DOMDocument();
          $curl = new LongTimeoutCurl();
          $return = $curl->get($url);
    			$doc->loadHTML($return);
    			$this->$xpath = new DOMXpath($doc);
    			 //echo $doc->saveHTML();

    			$url = $xpath->query("//*[contains(@class, 'list-unstyled')]");
    			$length = $url->item(0)->childNodes->length;

    			for ($i=0, $j=0; $i < $length/2 ;$j= $j+2, $i++)
    			{
    			 	$len = $url->item(0)->childNodes->item($j)->childNodes->length;
    			 	$len2 = $url->item(0)->childNodes->item($j)->childNodes->item($len-2)->childNodes->length;
    			 	$len3 = $url->item(0)->childNodes->item($j)->childNodes->item($len-2)->childNodes->item($len2-2)->childNodes->length;


    			 	$text = $url->item(0)->childNodes->item($j)->childNodes->item($len-2)->childNodes->item($len2-2)->childNodes->item($len3-1)->textContent;
    			 	if ($text =='')
    			 	{
    			 		continue;
    			 	}
    			 	$len4 = $url->item(0)->childNodes->item($j)->childNodes->item($len-2)->childNodes->item($len2-2)->childNodes->item(7)->childNodes->length;
    			 	$url1 = $url->item(0)->childNodes->item($j)->childNodes->item($len-2)->childNodes->item($len2-2)->childNodes->item($len3-1)->childNodes->item($len4-2)->childNodes->item(1)->attributes->item(0)->nodeValue;
    				$url1 = 'http://search.unisa.edu.au' .$url1;
    				$links[] = $url1;
    			}
    			$url = $xpath->query("//*[contains(@class, 'pagination pagination-lg')]");

    			$len4 = $url->item(0)->childNodes->length;
    			$url2 = $url->item(0)->childNodes->item($len4-2)->childNodes->item(0)->attributes->item(0)->textContent;
    			$url2 = 'http://search.unisa.edu.au/s/' .$url2;
    			if($url3 == $url2)
    				break;
    			$url = $url2;
  		}

    	return $links;
    }
}


?>
