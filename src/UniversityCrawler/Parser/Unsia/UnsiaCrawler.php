<?php

namespace UniversityCrawler\Parser\Unsia;

use App\Academics;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class UnsiaCrawler
{
    /**
     * @var UnsiaDirectoryPage
     */
    private $UnsiaDirectoryPage;
    /**
     * @var UnsiaPage
     */
    private $UnsiaPage;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(QutDirectoryPage $UnsiaDirectoryPage, QutPage $UnsiaPage, $debug = false)
    {
        $this->UnsiaDirectoryPage = $UnsiaDirectoryPage;
        $this->UnsiaPage = $UnsiaPage;
        $this->debug = $debug;
    }

    public function process()
    {

        $url = "http://search.unisa.edu.au/s/search.html?collection=people&query=&f.Staff%7Cstaff1=academic";
        
        $links = $this->UnsiaDirectoryPage->getLinks($url);
        if (empty($links)) 
        {
            PageParser::log('No links found', $this->debug);
            throw new NoDataException();
        }
        PageParser::log(count($links) . ' links found', $this->debug);

        foreach ($links as $link) 
        {
            try 
            {
                $this->saveToDb($this->UnsiaPage->parse($link));

            } 
            catch (\Exception $e) 
            {
                PageParser::log($e->getMessage(), $this->debug);
            }
        }
    }

    private function saveToDb($data)
    {
        $academic = new Academics();
        $academic->setRawAttributes($data);
        $academic->type = 'Unsia';
        $academic->save();
    }
}
