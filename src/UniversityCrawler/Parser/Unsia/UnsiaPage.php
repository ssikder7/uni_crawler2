<?php

namespace UniversityCrawler\Parser\Unsia;

use stringEncode\Exception;
use UniversityCrawler\Parser\PageParser;


class UnsiaPage extends PageParser
{
  protected $mapping = [
    'name' => [],
    'title' => [],
    'email' => [],
    'faculty_department_name' => [],
    'phone_number' => [],
    'qualifications' => [],
    'publication_record' => [],

  ];
  protected $xpath;

  public function parse($url)
  {
    PageParser::log("Processing $url", $this->debug);
    //$this->dom->loadFromUrl($url, [], $this->curl);

    //init DOMXpath
    libxml_use_internal_errors(true);
    libxml_disable_entity_loader(true);
    $doc = new DOMDocument();
    $curl = new LongTimeoutCurl();
    $return = $curl->get($url);
    $doc->loadHTML($return);
    $this->xpath = new DOMXpath($doc);

    $data = [
        'university_name' => 'University of South Australia',
        'url' => $url
    ];



    foreach ($this->mapping as $name => $options)
    {
      //phone_number => getPhoneNumber
      $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

      if (method_exists($this, $methodName))
      {
        $data[$name] = $this->$methodName();
      }
      else
      {
        throw new Exception('No method for getting ' . $name);
      }
    }

    return $data;
  }

  private function getFacultyDepartmentName()
  {
    $facaulty = $this->xpath->query("//*[contains(@class, 'inline-list key-value-content')]");
    if($facaulty->length == 0 ) return '';
    $Facaulty = $facaulty->item(1)->childNodes->item(0)->textContent;//->textContent;
    return $Facaulty;

  }

  private function getName()
  {
    $name = $this->xpath->query("//*[contains(@class, 'h2 profile-header-heading')]");
    if($name->length == 0 ) return '';
    $name =  $name->item(0)->textContent;
    return $name;
  }

  private function getTitle()
  {
    $title = $this->xpath->query("//*[contains(@class, 'inline-list key-value-content')]");
    if($title->length == 0 ) return '';
    $Title = $title->item(0)->textContent;
    return $Title;
  }

  private function getEmail()
  {
    $email = $this->xpath->query("//*[contains(@class, 'inline-list')]");
    if($email->length == 0 ) return '';
    $email = $email->item(5)->textContent;
    $email = substr( $email, 6);
    return $email;
  }

	private function getPhoneNumber()
  {
    $phone = $this->xpath->query("//*[contains(@class, 'inline-list key-value-content')]");
    if($phone->length == 0 ) return '';
    $phone = $phone->item(1)->childNodes->item(2)->textContent;
    $phone = substr( $phone, 4);
    return $phone;
  }

  private function getQualifications()
  {
    $qualifications = $this->xpath->query("//*[contains(@class, 'data-set')]");
    if($qualifications->length == 0 ) return '';
    $length = $qualifications->item(0)->childNodes->length;
    $qualificationsList = array();
    for ($i=0; $i <$length ; $i=$i+2)
    {
    	$qualificationsList[] = $qualifications->item(0)->childNodes->item($i)->textContent;
    }

    return $qualificationsList;
  }

  private function getPublicationRecord()
  {
    $publication = $this->xpath->query("//*[contains(@class, 'layout-section-footer')]");
    if($publication->length == 0 ) return '';
    $length = $publication->item(1)->childNodes->item(1)->childNodes->length;
    $publication = $this->xpath->query("//*[contains(@class, 'modal')]");

    for ($i=0,$j=7; $i <$length/2;$j=$j+2, $i++)
    {
		  $temp = $publication->item(1)->childNodes->item(19)->childNodes->item($j)->childNodes->item(5)->textContent;
		  $pub[] = $temp;
		}
	 return $pub;
  }
}

?>
