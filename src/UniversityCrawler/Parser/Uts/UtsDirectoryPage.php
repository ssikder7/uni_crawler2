<?php

namespace UniversityCrawler\Parser\Uts;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class UtsDirectoryPage extends PageParser
{
    protected $xpath;
    public function getLinks($directoryUrl)
    {
        $domain = "https://www.uts.edu.au";
        $url = $directoryUrl['url'];
        $faculty = $directoryUrl['faculty'];
        $links = array();

        //init DOMXpath
        libxml_use_internal_errors(true);
        libxml_disable_entity_loader(true);
        $doc = new DOMDocument();
        $curl = new LongTimeoutCurl();
        $return = $curl->get($url);
        $doc->loadHTML($return);
        $this->xpath = new DOMXpath($doc);

        $table = $this->xpath->query("//*[contains(@class, 'views-table')]");
        $len = $table->item(0)->childNodes->item(1)->childNodes->length;

        for ($x=0;$x<$len;$x++) {
          $eachLink = $domain.$table->item(0)->childNodes->item(1)->childNodes->item($x)->childNodes->item(0)->childNodes->item(1)->attributes->item(0)->textContent;
          $links[] = array("url"=>$eachLink, "faculty"=>$faculty);
        }

        $nextLink = '';
        $next = $this->xpath->query("//*[contains(@class, 'pager-next')]");
        if ($next->length > 0) {
          $nextLink = $next->item(0)->childNodes->item(0)->attributes->item(1)->value;
          if ($nextLink != '') {
            $nextLink = $domain.$nextLink;
            $links2 = array();
            $links2 = $this->getLinks(array("url"=>$nextLink, "faculty"=>$faculty));
            foreach ($links2 as $v) {
              $links[] = $v;
            }
          }
        }

        return $links;
    }
}
