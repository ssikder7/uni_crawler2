<?php

namespace UniversityCrawler\Parser\Uts;

use stringEncode\Exception;
use UniversityCrawler\Parser\PageParser;

class UtsPage extends PageParser
{
  protected $mapping = [
      'name' => [],
      'title' => [],
      'qualifications' => [],
      'faculty_department_name' => [],
      'phone_number' => [],
      'email' => [],
      'publication_record' => [],
      'other' => []
  ];
  protected $xpath;
  protected $faculty;
  public function parse($obj)
  {
      $url = $obj['url'];
      $this->faculty = $obj['faculty'];

      PageParser::log("Processing $url", $this->debug);
      //$this->dom->loadFromUrl($url, [], $this->curl);

      //init DOMXpath
      libxml_use_internal_errors(true);
      libxml_disable_entity_loader(true);
      $doc = new DOMDocument();
      $curl = new LongTimeoutCurl();
      $return = $curl->get($url);
      $doc->loadHTML($return);
      $this->xpath = new DOMXpath($doc);

      $data = [
          'university_name' => 'University of Technology, Sydney',
          'url' => $url
      ];

      foreach ($this->mapping as $name => $options) {
          //phone_number => getPhoneNumber
          $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

          if (method_exists($this, $methodName)) {
              $data[$name] = $this->$methodName();
          } else {
              throw new Exception('No method for getting ' . $name);
          }
      }

      return $data;
  }

  private function getFacultyDepartmentName() {
    return $this->faculty;
  }

  private function getName() {
    $name = $this->xpath->query("//*[contains(@class, 'pane-content')]");
    $name = $name->item('0')->textContent;
    return $name;
  }
  private function getEmail() {
    $email = $this->xpath->query("//*[contains(@class, 'field-staff-email odd delta-0')]");
    $email = $email->item('0')->textContent;
    return $email;
  }
  private function getPhoneNumber() {
    $phone = $this->xpath->query("//*[contains(@class, 'field-staff-phone odd delta-0')]");
    $phone = $phone->item('0')->textContent;
    return $phone;
  }
  private function getTitle() {
    $positionTitles = $this->xpath->query("//*[contains(@class,'field-staff-positions')]");
    $positionTitleList = array();
    for ($x=1;$x<$positionTitles->length;$x++) {
      $positionTitleList[] = $positionTitles->item($x)->textContent;
    }

    $memberships = $this->xpath->query("//*[contains(@class,'field-staff-memberships')]");
    //$membershipsList = array();
    for ($x=1;$x<$memberships->length;$x++) {
      $positionTitleList[] = $memberships->item($x)->textContent;
    }

    return $positionTitleList;
  }
  private function getQualifications() {
    $qualifications = $this->xpath->query("//*[contains(@class,'field-staff-qualifications')]");
    $qualificationsList = array();
    for ($x=1;$x<$qualifications->length;$x++) {
      $qualificationsList[] = $qualifications->item($x)->textContent;
    }
    return $qualificationsList;
  }
  private function getPublicationRecord() {
    $publications = $this->xpath->query("//*[contains(@class,'category-group')]");
    $publicationsList = array();
    //loop for category
    for ($x=0;$x<$publications->length;$x++) {
      $len = $publications->item($x)->childNodes->length;
      //combines all into 1 array
      for ($y=3;$y<$len;$y=$y+2) {
        $publicationsList[] = $publications->item($x)->childNodes->item($y)->textContent;
      }
    }
    return $publicationsList;
  }
  private function getOther() {
    $other1 = $this->xpath->query("//*[contains(@class,'description_inner')]");
    $otherList = array();
    $len = $other1->item(0)->childNodes->length;
    for ($x=0;$x<$len;$x++) {
      if ($other1->item(0)->childNodes->item($x)->nodeName == "h3") {
        $title = $other1->item(0)->childNodes->item($x)->textContent;
        $content = $other1->item(0)->childNodes->item($x+2)->textContent;
        $otherList[$title] = $content;
      }
    }
    return $otherList;
  }

}
