<?php

namespace UniversityCrawler\Parser\Uts;

use App\Academics;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class UtsCrawler
{
    /**
     * @var QutDirectoryPage
     */
    private $utsDirectoryPage;
    /**
     * @var QutPage
     */
    private $utsPage;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(UtsDirectoryPage $utsDirectoryPage, UtsPage $utsPage, $debug = false)
    {
        $this->utsDirectoryPage = $utsDirectoryPage;
        $this->utsPage = $utsPage;
        $this->debug = $debug;
    }

    public function process()
    {
        $obj = array(
          [ "url"=>"https://www.uts.edu.au/about/faculty-arts-and-social-sciences/academic-staff",
            "faculty"=>"Faculty of Arts and Social Sciences"
          ],
          [
            "url"=>"http://www.uts.edu.au/about/faculty-law/academic-staff",
            "faculty"=>"Faculty of Law"
          ],
          [
            "url"=>"https://www.uts.edu.au/about/faculty-health/academic-staff",
            "faculty"=>"Faculty of Health"
          ],
          [
            "url"=>"https://www.uts.edu.au/about/uts-business-school/academic-staff",
            "faculty"=>"UTS Business School"
          ],
          [
            "url"=>"https://www.uts.edu.au/about/faculty-design-architecture-and-building/dab-academic-staff-0",
            "faculty"=>"Faculty of Design, Architecture and Building"
          ],
          [
            "url"=>"https://www.uts.edu.au/about/faculty-engineering-and-information-technology/feit-staff",
            "faculty"=>"Faculty of Engineering and Information Technology"
          ],
          [
            "url"=>"https://www.uts.edu.au/about/graduate-school-health/academic-staff",
            "faculty"=>"Graduate School of Health"
          ]
        );

        foreach ($obj as $k=>$v) {
          PageParser::log('Starting UTS - '.$v['faculty'], $debug);
          $links = $this->utsDirectoryPage->getLinks($v['url']);
          if (empty($links)) {
              PageParser::log('No links found', $this->debug);
              throw new NoDataException();
          }
          PageParser::log(count($links) . ' links found', $this->debug);

          foreach ($links as $link) {
              try {
                  $this->saveToDb($this->utsPage->parse($link));
              } catch (\Exception $e) {
                  PageParser::log($e->getMessage(), $this->debug);
              }
          }
        }

    }

    private function saveToDb($data)
    {
        $academic = new Academics();
        $academic->setRawAttributes($data);
        $academic->type = 'uts';
        $academic->save();
    }
}
