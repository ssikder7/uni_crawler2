<?php

namespace UniversityCrawler\Parser\UnOfAdelaida;

use UniversityCrawler\Parser\PageParser;

class UnOfAdelaidaPage extends PageParser
{
    protected $mapping = [
        'name' => [
            'type' => 'dom',
            'path' => '#homepage-main h1',
        ],
        'title' => [
            'regex' => '/<th>Position<\/th> <td>(.*?)<\/td>/'
        ],
        'qualifications' => [
            '/<a name="Qualifications"><\/a><h2>Qualifications<\/h2>(.*?)<a name=/',
            '/<a name="Qualifications"><\/a><h2>Qualifications<\/h2>(.*?)<p class="caption">/'
        ],
        'faculty_department_name' => [
            'building' => '/<th>Building<\/th> <td> <a href="(.*?)">(.*?)<\/a> <\/td>/',
            'floor_room' => '/<th>Floor\/Room<\/th> <td>(.*?)<\/td>/',
            'campus' => '/<th>Campus<\/th> <td>(.*?)<\/td>/'
        ],
        'phone_number' => [
            'regex' => '/<th>Telephone<\/th> <td> <a href="(.*?)"><strong>(.*?)<\/strong><\/a>/'
        ],
        'publication_record' => [
            '/<a name="Publications"><\/a><h2>Publications<\/h2>(.*?)<a name=/',
            '/<a name="Publications"><\/a><h2>Publications<\/h2>(.*?)<p class="caption">/'
        ],

        'other' => [
            'Org Unit' => '/<tr><th>Org Unit<\/th> <td> <a href="(.*?)">(.*?)<\/a> <\/td> <\/tr>/',
            'Biography/ Background' => [
                '/<a name="Biography\/ Background"><\/a><h2>Biography\/ Background<\/h2>(.*?)<a name=/',
                '/<a name="Biography\/ Background"><\/a><h2>Biography\/ Background<\/h2>(.*?)<p class="caption">/'
            ],
            'Research Interests' => [
                '/<a name="Research Interests"><\/a><h2>Research Interests<\/h2>(.*?)<a name=/',
                '/<a name="Research Interests"><\/a><h2>Research Interests<\/h2>(.*?)<p class="caption">/'
            ],
            'Teaching Interests' => [
                '/<a name="Teaching Interests"><\/a><h2>Teaching Interests<\/h2>(.*?)<a name=/',
                '/<a name="Teaching Interests"><\/a><h2>Teaching Interests<\/h2>(.*?)<p class="caption">/'
            ],
            'Professional Associations' => [
                '/<a name="Professional Associations"><\/a><h2>Professional Associations<\/h2>(.*?)<a name=/',
                '/<a name="Professional Associations"><\/a><h2>Professional Associations<\/h2>(.*?)<p class="caption">/'
            ],
            'Awards & Achievements' => [
                '/<a name="Awards & Achievements"><\/a><h2>Awards & Achievements<\/h2>(.*?)<a name=/',
                '/<a name="Awards & Achievements"><\/a><h2>Awards & Achievements<\/h2>(.*?)<p class="caption">/'
            ],
            'Files' => [
                '/<a name="files"><\/a><h2>Files<\/h2>(.*?)<a name=/',
                '/<a name="files"><\/a><h2>Files<\/h2>(.*?)<p class="caption">/'
            ],
            'Expertise' => [
                '/<a name="Expertise"><\/a><h2>Expertise for Media Contact<\/h2>(.*?)<a name=/',
                '/<a name="Expertise"><\/a><h2>Expertise for Media Contact<\/h2>(.*?)<p class="caption">/'
            ],
            'Research Funding' => [
                '/<a name="Research Funding"><\/a><h2>Research Funding<\/h2>(.*?)<a name=/',
                '/<a name="Research Funding"><\/a><h2>Research Funding<\/h2>(.*?)<p class="caption">/'
            ]
        ]
    ];

    private $url;

    public function parse($url)
    {
        $this->url = $url;

        PageParser::log("Processing $url", $this->debug);
        $this->dom->loadFromUrl($url, [], $this->curl);

        $data = [
            'university_name' => 'The University of Adelaide',
            'url' => $url,
            'type' => UnOfAdelaidaCrawler::TYPE
        ];

        foreach ($this->mapping as $name => $options) {
            //phone_number => getPhoneNumber
            $methodName = 'get' . str_replace(' ', '', ucfirst(str_replace('_', ' ', $name)));

            if (method_exists($this, $methodName)) {
                $data[$name] = $this->$methodName();
            } else {
                throw new \Exception('No method for getting ' . $name);
            }
        }

        return $data;
    }

    private function getFromDom($path)
    {
        $data = [];
        $elements = $this->dom->find($path);
        foreach ($elements as $element) {
            $data[] = strip_tags(str_replace('>', '> ', $element->outerHtml));
        }

        return $data;
    }

    private function getName()
    {
        $name = $this->getSingleDomItemByPath($this->mapping['name']['path']);
        if (!$name) {
            throw new \Exception('No name for ' . $this->url);
        }

        return $this->prepareString($name);
    }

    private function getQualifications()
    {
        return $this->prepareString(
            $this->findByRegexVariants(
                $this->getHtml(), $this->mapping['qualifications']
            )
        );
    }

    private function getTitle()
    {
        $html = $this->getHtml();

        $regex = $this->mapping['title']['regex'];

        if (preg_match($regex, $html, $matches)) {
            return $matches[1];
        } else {
            return '';
        }
    }

    private function getPhoneNumber()
    {
        $html = $this->getHtml();

        $regex = $this->mapping['phone_number']['regex'];

        if (preg_match($regex, $html, $matches)) {
            return $matches[2];
        } else {
            return '';
        }
    }

    private function getSingleDomItemByPath($path)
    {
        $item = $this->getFromDom($path);
        return isset($item[0]) ? $item[0] : '';
    }

    private function getFacultyDepartmentName()
    {
        $html = $this->getHtml();
        $data = [];

        if (preg_match($this->mapping['faculty_department_name']['building'], $html, $matches)) {
            $data[] = "Building: " . $matches[2];
        }

        if (preg_match($this->mapping['faculty_department_name']['floor_room'], $html, $matches)) {
            $data[] = "Floor/Room: " . $matches[1];
        }

        if (preg_match($this->mapping['faculty_department_name']['campus'], $html, $matches)) {
            $data[] = "Campus: " . $matches[1];
        }

        return implode("; ", $data);

    }

    private function getHtml()
    {
        $html = $this->dom->outerHtml;
        $html = preg_replace("/\r|\n/", "", $html);
        return str_replace('&nbsp;', ' ', $html);
    }

    private function getPublicationRecord()
    {
        return $this->prepareString(
            $this->findByRegexVariants(
                $this->getHtml(), $this->mapping['publication_record']
            )
        );
    }

    public function getOther()
    {
        $data = [];
        foreach ($this->mapping['other'] as $name => $regexVariants) {
            $item = $this->findByRegexVariants($this->getHtml(), $regexVariants);
            if ($item) {
                $data[] = $name . ": " . $this->prepareString($item);
            }
        }

        return $this->prepareString(implode('; ', $data));
    }

    private function prepareString($string, $max = 65000)
    {
        $string = strip_tags($string);
        $string = str_replace("  ", ' ', $string);
        $string = str_replace("  ", ' ', $string);

        return substr($string, 0, $max);
    }

    private function findByRegexVariants($html, $variants)
    {
        if (!is_array($variants)) {
            $variants = [$variants];
        }

        foreach ($variants as $variant) {
            if (preg_match($variant, $html, $matches)) {
                return end($matches);
            }
        }

        return '';
    }
}

