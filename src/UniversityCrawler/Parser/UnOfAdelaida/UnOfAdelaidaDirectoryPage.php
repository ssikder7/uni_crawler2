<?php

namespace UniversityCrawler\Parser\UnOfAdelaida;

use PHPHtmlParser\Dom\HtmlNode;
use UniversityCrawler\Parser\PageParser;

class UnOfAdelaidaDirectoryPage extends PageParser
{
    public function getLinks($directoryUrl)
    {
        $links = [];
        $this->dom->loadFromUrl($directoryUrl, [], $this->curl);
        $linkElements = $this->dom->find('#bztable1 tr:nth-child(2) td:nth-child(2) a');

        if (!count($linkElements)) {
            return [];
        }

        /** @var HtmlNode $element */
        foreach ( $linkElements as $element ) {
            $link = $element->getAttribute('href');
            if (strpos($link, 'mailto:') === 0) {
                continue;
            }
            $links[] = "http://www.adelaide.edu.au" . trim($link);
        }

        $links = array_unique($links, SORT_STRING);
        sort($links);

        return $links;
    }
}