<?php

namespace UniversityCrawler\Parser\UnOfAdelaida;

use App\Academics;
use App\FailedUrls;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;

class UnOfAdelaidaCrawler
{
    const TYPE = 'unofadelaida';

    /**
     * @var UnOfAdelaidaDirectoryPage
     */
    private $unOfAdelaidaDirectoryPage;
    /**
     * @var UnOfAdelaidaPage
     */
    private $unOfAdelaida;
    /**
     * @var bool
     */
    private $debug;

    public function __construct(UnOfAdelaidaDirectoryPage $unOfAdelaidaDirectoryPage, UnOfAdelaidaPage $unOfAdelaidaPage, $debug = false)
    {
        $this->unOfAdelaidaDirectoryPage = $unOfAdelaidaDirectoryPage;
        $this->unOfAdelaida = $unOfAdelaidaPage;
        $this->debug = $debug;
    }

    public function process($letter, $page)
    {
        $url = "http://www.adelaide.edu.au/directory/atoz/".strtoupper($letter).".html?dsn=directory.phonebook;orderby=last%2Cfirst%2Cposition_n;letter=".strtoupper($letter).";m=atoz;page=".$page;
        PageParser::log($url, $this->debug);

        $links = $this->unOfAdelaidaDirectoryPage->getLinks($url);

        if (empty($links)) {
            PageParser::log('No links found', $this->debug);
            throw new NoDataException();
        }
        PageParser::log(count($links) . ' links found', $this->debug);

        foreach ($links as $link) {
            try {
                $exists = Academics::where('url', $link)->get();
                if (count($exists)) {
                    continue;
                }
                $this->saveToDb($this->unOfAdelaida->parse($link));
            } catch (\Exception $e) {
                $this->saveFailedUrl($link, self::TYPE, $e->getMessage());
                PageParser::log($e->getMessage(), $this->debug);
            }
        }
    }

    private function saveToDb($data)
    {
        $academic = new Academics();
        $academic->setRawAttributes($data);
        $academic->save();
    }

    private function saveFailedUrl($url, $type, $reason)
    {
        if (count(FailedUrls::where('url', $url)->get())) {
            return;
        }
        $failedUrl = new FailedUrls();
        $failedUrl->url = $url;
        $failedUrl->type = $type;
        $failedUrl->reason = $reason;
        $failedUrl->save();
    }
}
