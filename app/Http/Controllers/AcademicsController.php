<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Academics;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use PHPHtmlParser\Dom;
use Session;
use UniversityCrawler\ES\Search;
use UniversityCrawler\Parser\ParserFactory;

class AcademicsController extends Controller
{

    private $pageSize = 25;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $academics = $this->searchAcademics($keyword);
        } else {
            $academics = Academics::paginate($this->pageSize);
        }

        return view('academics.index', ['academics' => $academics, 'keyword' => $keyword, 'stats' => $this->getAcademicsStats()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('academics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Academics::create($requestData);

        \Session::flash('info', 'Academics added!');

        return redirect('academics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return redirect('academics/' . $id . '/edit');

        $academic = Academics::findOrFail($id);

        return view('academics.show', compact('academic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $academic = Academics::findOrFail($id);

        return view('academics.edit', compact('academic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        unset($requestData['_method']);
        unset($requestData['_token']);
        
        $academic = Academics::findOrFail($id);
        $academic->update($requestData);

        \Session::flash('info', 'Academics updated!');

        return redirect('academics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Academics::destroy($id);

        \Session::flash('info', 'Academics deleted!');

        return redirect('academics');
    }

    private function searchAcademics($keyword)
    {
        $search  = new Search();

        $response = $search->globalAcademicSearchByKeyword($keyword);

        return $this->getAcademicsBySearchResult($response);

    }

    public function recrawl($id)
    {
        /** @var Academics $academic */
        $academic = Academics::findOrFail($id);
        if ($academic) {
            $pageParser = ParserFactory::getParser($academic->type);
            $urls = explode(';', $academic->url);
            if (count($urls) > 1) {
                \Session::flash('info', 'Academic can not be recrawled from multiple sources');
                return redirect('academics/' . $id . '/edit');
            }
            $academic->update($pageParser->parse($academic->url));
        }

        \Session::flash('info', 'Academic updated!');

        return redirect('academics/' . $id . '/edit');
    }

    public function advancedSearch(Request $request)
    {
        $search = $request->get('search');

        if (!empty($search)) {
            $academics = $this->searchAcademicsAdvanced($search);
        } else {
            $academics = Academics::paginate($this->pageSize);
        }

        return view('academics.advancedSearch', [
            'academics' => $academics,
            'search' => $search,
            'universities' => $this->getUniversitiesList()
        ]);
    }

    private function searchAcademicsAdvanced($searchParams)
    {
        $search  = new Search();
        $response = $search->advancedAcademicSearch($searchParams);
        return $this->getAcademicsBySearchResult($response);
    }

    /**
     * @param $response
     * @return Paginator
     */
    private function getAcademicsBySearchResult($response)
    {
        $ids = [];
        foreach ($response['hits']['hits'] as $item) {
            $ids[] = $item['_id'];
        }

        /** @var Paginator $academics */
        $academics = Academics::whereIn('id', $ids)->paginate($this->pageSize);
        $orderedAcademics = [];
        foreach ($academics as $academic) {
            $orderedAcademics[$academic->id] = $academic;
        }

        $orderedAcademics = array_replace(array_flip($ids), $orderedAcademics);

        foreach ($orderedAcademics as $id => $academic) {
            if (!($academic instanceof Academics)) {
                unset($orderedAcademics[$id]);
            }
        }

        $academics->setCollection(new Collection($orderedAcademics));

        return $academics;
    }

    private function getAcademicsStats()
    {
        return Academics::getStats();
    }

    public function statistics()
    {
        return view('academics.statistics', ['stats' => $this->getAcademicsStats()]);
    }

    private function getUniversitiesList()
    {
        $list = [];
        $universities = Academics::getUniversities();
        foreach ($universities as $university) {
            $list[$university->university_name] = $university->university_name;
        }

        return $list;
    }
}
