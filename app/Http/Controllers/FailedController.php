<?php

namespace App\Http\Controllers;

use App\FailedUrls;
use App\Http\Requests;

use App\Academics;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use Session;
use UniversityCrawler\Parser\ParserFactory;

class FailedController extends Controller
{

    private $pageSize = 25;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $urls = FailedUrls::paginate($this->pageSize);

        return view('failed.index', ['urls' => $urls]);
    }

    public function delete($id)
    {
        FailedUrls::destroy($id);
        \Session::flash('info', 'Url removed');
        return redirect('failed');
    }

    public function recrawl(Request $request, $id)
    {
        $failedUrl = FailedUrls::findOrFail($id);
        if ($failedUrl) {
            $pageParser = ParserFactory::getParser($failedUrl->type);
            try {
                $data = $pageParser->parse($failedUrl->url);
                $academic = new Academics();
                $academic->setRawAttributes($data);
                $academic->type = $failedUrl->type;
                $academic->save();
                $this->delete($id);
                \Session::flash('info', 'Url recrawled');
                return redirect('academics/' . $id . '/edit');
            } catch (\Exception $e) {
                \Session::flash('danger', $e->getMessage());
            }
        }

        return redirect('failed');
    }

}
