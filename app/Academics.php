<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use UniversityCrawler\ES\AcademicIndexer;
use Illuminate\Support\Facades\DB;

class Academics extends Model
{
    protected static $unguarded = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'academics';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public static function getStats()
    {
        $data = [];
        $univercities = self::getUniversities();
        foreach ($univercities as $univercity) {
            $count = DB::select('SELECT count(*) as count FROM `academics` WHERE university_name="'.$univercity->university_name.'"');
            $data[$univercity->university_name] = $count[0]->count;
        }

        return $data;
    }

    public static function destroy($id)
    {
        AcademicIndexer::delete($id);
        return parent::destroy($id);
    }

    public static function getUniversities()
    {
        return DB::select('SELECT DISTINCT university_name FROM `academics`');
    }

    public function delete()
    {
        AcademicIndexer::delete($this->id);
        return parent::delete();
    }

    public function save(array $options = [])
    {
        $saved = parent::save($options);
        if ($saved) {
            AcademicIndexer::index($this);
        }

        return $saved;
    }

    public function update(array $attributes = [], array $options = [])
    {
        $saved = parent::update($attributes, $options);
        if ($saved) {
            AcademicIndexer::index($this);
        }

        return $saved;
    }
}
