<?php

namespace App\Console;

use App\Console\Commands\CrawlQut;
use App\Console\Commands\CrawlUnOfAdelaida;
use App\Console\Commands\CrawlUnOfAdelaida2;
use App\Console\Commands\CrawlUqEduAu;
use App\Console\Commands\IndexAcademics;
use App\Console\Commands\RemoveDuplicates;
use App\Console\Commands\ShapeDataset;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        IndexAcademics::class,
        CrawlQut::class,
        CrawlUnOfAdelaida::class,
        CrawlUnOfAdelaida2::class,
        CrawlUqEduAu::class,
        ShapeDataset::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
