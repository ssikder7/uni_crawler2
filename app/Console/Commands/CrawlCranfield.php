<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\Cranfield\CranfieldCrawler;
use UniversityCrawler\Parser\Cranfield\CranfieldDirectoryPage;
use UniversityCrawler\Parser\Cranfield\CranfieldPage;

class CrawlCranfield extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:Cranfield';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from Cranfield';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $debug = true;

        $directoryParser = new CranfieldDirectoryPage(null, null, $debug);
        $pageParser = new CranfieldPage(null, null, $debug);

        $CranfieldCrawler = new CranfieldCrawler($directoryParser, $pageParser, $debug);

        
        PageParser::log("Starting program for Cranfield", $debug);
        try 
        {
            $CranfieldCrawler->process();
        }
        catch (NoDataException $e) 
        {
            break;
        } 
        catch (\Exception $e) 
        {
            PageParser::log($e->getMessage(), $debug);
                
        }
    }
}

