<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\Uts\UtsCrawler;
use UniversityCrawler\Parser\Uts\UtsDirectoryPage;
use UniversityCrawler\Parser\Uts\UtsPage;

class CrawlUts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:uts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from UTS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$letters = range('a', 'z');
        //$pages = range(1, 100);

        $debug = true;

        $directoryParser = new UtsDirectoryPage(null, null, $debug);
        $pageParser = new UtsPage(null, null, $debug);

        $utsCrawler = new UtsCrawler($directoryParser, $pageParser, $debug);

        PageParser::log('Starting UTS', $debug);
        try {
            $qutCrawler->process($letter, $page);
        } catch (NoDataException $e) {
            break;
        } catch (\Exception $e) {
            PageParser::log($e->getMessage(), $debug);
        }
    }
}
