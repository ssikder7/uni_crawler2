<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\UqEduAu\UqEduAuCrawler;
use UniversityCrawler\Parser\UqEduAu\UqEduAuDirectoryPage;
use UniversityCrawler\Parser\UqEduAu\UqEduAuPage;

class CrawlUqEduAu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:uqeduau';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from The University of Queensland';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $debug = true;
        $directoryParser = new UqEduAuDirectoryPage(null, null, $debug);
        $pageParser = new UqEduAuPage(null, null, $debug);
        $crawler = new UqEduAuCrawler($directoryParser, $pageParser, $debug);

        //$fieldOfResearchLinks = $directoryParser->getFieldsOfResearchLinks();
        $fieldOfResearchLinks = $directoryParser->getOrganizationsLinks();

        foreach ($fieldOfResearchLinks as $link) {
            PageParser::log("Starting $link", $debug);
            try {
                $crawler->process($link);
            } catch (NoDataException $e) {
                break;
            } catch (\Exception $e) {
                PageParser::log($e->getMessage(), $debug);
            }
        }
    }
}
