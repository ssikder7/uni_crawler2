<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\Unsia\UnsiaCrawler;
use UniversityCrawler\Parser\Unsia\UnsiaDirectoryPage;
use UniversityCrawler\Parser\Unsia\UnsiaPage;

class CrawlUnsia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:Unsia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from Unsia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $debug = true;

        $directoryParser = new UnsiaDirectoryPage(null, null, $debug);
        $pageParser = new UnsiaPage(null, null, $debug);

        $UnsiaCrawler = new UnsiaCrawler($directoryParser, $pageParser, $debug);

        
        PageParser::log("Starting program for Unsia", $debug);
        try 
        {
            $UnsiaCrawler->process();
        } 
        catch (NoDataException $e) 
        {
            break;
        } 
        catch (\Exception $e) 
        {
            PageParser::log($e->getMessage(), $debug);
                
        }
    }
}

