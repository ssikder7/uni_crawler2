<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\Qut\QutCrawler;
use UniversityCrawler\Parser\Qut\QutDirectoryPage;
use UniversityCrawler\Parser\Qut\QutPage;

class CrawlQut extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:qut';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from QUT';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $letters = range('a', 'z');
        $pages = range(1, 100);

        $debug = true;

        $directoryParser = new QutDirectoryPage(null, null, $debug);
        $pageParser = new QutPage(null, null, $debug);

        $qutCrawler = new QutCrawler($directoryParser, $pageParser, $debug);

        foreach ($letters as $letter) {
            PageParser::log('Starting letter ' . $letter, $debug);
            foreach ($pages as $page) {
                PageParser::log("Starting page $page for letter $letter", $debug);
                try {
                    $qutCrawler->process($letter, $page);
                } catch (NoDataException $e) {
                    break;
                } catch (\Exception $e) {
                    PageParser::log($e->getMessage(), $debug);
                }
            }
        }
    }
}
