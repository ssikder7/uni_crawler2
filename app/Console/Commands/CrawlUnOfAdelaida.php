<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\UnOfAdelaida\UnOfAdelaidaCrawler;
use UniversityCrawler\Parser\UnOfAdelaida\UnOfAdelaidaDirectoryPage;
use UniversityCrawler\Parser\UnOfAdelaida\UnOfAdelaidaPage;

class CrawlUnOfAdelaida extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:unofadelaida';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from University Of Adelaida';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $letters = range('a', 'z');
        $pages = range(1, 800);

        $debug = true;

        $directoryParser = new UnOfAdelaidaDirectoryPage(null, null, $debug);
        $pageParser = new UnOfAdelaidaPage(null, null, $debug);


        //$url = 'http://www.adelaide.edu.au/directory/alan.collins';
        //$url = 'http://www.adelaide.edu.au/directory/phil.saj';
        //$url = "http://www.adelaide.edu.au/directory/rebecca.laforgia";
        //$data = $pageParser->parse($url);
        //print_r($data);
        //exit;

        $qutCrawler = new UnOfAdelaidaCrawler($directoryParser, $pageParser, $debug);

        foreach ($letters as $letter) {
            PageParser::log('Starting letter ' . $letter, $debug);
            foreach ($pages as $page) {
                PageParser::log("Starting page $page for letter $letter", $debug);
                try {
                    $qutCrawler->process($letter, $page);
                } catch (NoDataException $e) {
                    break;
                } catch (\Exception $e) {
                    PageParser::log($e->getMessage(), $debug);
                }
            }
        }
    }
}
