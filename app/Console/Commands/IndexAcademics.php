<?php

namespace App\Console\Commands;

use App\Academics;
use Cviebrock\LaravelElasticsearch\Facade;
use Illuminate\Console\Command;
use UniversityCrawler\ES\AcademicIndexer;

class IndexAcademics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'esindex:academics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for indexing academics in ES';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $perPage = 100;

        foreach (range(1, 100000) as $page) {
            $offset = ($page - 1) * $perPage;
            $academics = Academics::limit($perPage)->offset($offset)->get();
            if (!count($academics)) {
                break;
            }
            /** @var Academics $academic */
            foreach ($academics as $academic) {
                print_r($academic->id . "\n");
                AcademicIndexer::index($academic);
            }
        }
    }
}
