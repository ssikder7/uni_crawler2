<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use UniversityCrawler\Parser\NoDataException;
use UniversityCrawler\Parser\PageParser;
use UniversityCrawler\Parser\UnOfAdelaida2\UnOfAdelaidaCrawler;
use UniversityCrawler\Parser\UnOfAdelaida2\UnOfAdelaidaDirectoryPage;
use UniversityCrawler\Parser\UnOfAdelaida2\UnOfAdelaidaPage;

class CrawlUnOfAdelaida2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:unofadelaida2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for academics from University Of Adelaida';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pages = range(0, 800);

        $debug = true;

        $directoryParser = new UnOfAdelaidaDirectoryPage(null, null, $debug);
        $pageParser = new UnOfAdelaidaPage(null, null, $debug);


//        $url = "https://researchers.adelaide.edu.au/profile/javen.shi";
//        $data = $pageParser->parse($url);
//        print_r($data);
//        exit;

        $qutCrawler = new UnOfAdelaidaCrawler($directoryParser, $pageParser, $debug);

        foreach ($pages as $page) {
            PageParser::log("Starting page $page", $debug);
            try {
                $qutCrawler->process($page);
            } catch (NoDataException $e) {
                break;
            } catch (\Exception $e) {
                PageParser::log($e->getMessage(), $debug);
            }
        }

    }
}
