<?php

namespace App\Console\Commands;

use App\Academics;
use App\FailedUrls;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ShapeDataset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:shape_dataset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for remove duplicates from DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->removeAcademics();
        $this->removeFailed();
        $this->cleanFailed();
    }

    private function removeAcademics()
    {
        $query = "SELECT url FROM `academics` GROUP BY url HAVING count(*) >1";
        $result = DB::select(DB::raw($query));
        print_r(count($result) . ' duplicated urls found' . "\n");
        foreach ($result as $url) {
            $academics = Academics::where('url', $url->url)->get();
            $count = count($academics);
            print_r($count . ' duplicates found for ' . $url->url . "\n");
            print_r('will leave ' . $academics[count($academics) - 1]->id . "\n");
            unset($academics[count($academics) - 1]);
            $delete = 0;
            foreach ($academics as $academic) {
                $academic->delete();
                $delete++;
            }
            print_r($delete . ' duplicates was removed' . "\n");
            print_r("\n");
        }
    }

    private function removeFailed()
    {
        $query = "SELECT url FROM `failed_urls` GROUP BY url HAVING count(*) >1";
        $result = DB::select(DB::raw($query));
        print_r(count($result) . ' duplicated urls found' . "\n");
        foreach ($result as $url) {
            $academics = FailedUrls::where('url', $url->url)->get();
            $count = count($academics);
            print_r($count . ' duplicates found for ' . $url->url . "\n");
            print_r('will leave ' . $academics[count($academics) - 1]->id . "\n");
            unset($academics[count($academics) - 1]);
            $delete = 0;
            foreach ($academics as $academic) {
                $academic->delete();
                $delete++;
            }
            print_r($delete . ' duplicates was removed' . "\n");
            print_r("\n");
        }
    }

    private function cleanFailed()
    {
        $query = "SELECT failed_urls.id FROM `failed_urls` JOIN academics on failed_urls.url = academics.url";
        $result = DB::select(DB::raw($query));
        print_r(count($result) . ' failed urls to remove found' . "\n");
        if (!count($result)) {
            return;
        }
        $ids = [];
        foreach ($result as $row) {
            $ids[] = $row->id;
        }

        $query = "DELETE FROM `failed_urls` where id in (".implode($ids, ',').")";
        $result = DB::delete(DB::raw($query));
    }
}
